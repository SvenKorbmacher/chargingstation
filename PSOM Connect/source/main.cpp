/****************************************************************************
**
** Copyright (C) 2012 Denis Shienkov <denis.shienkov@gmail.com>
** Copyright (C) 2012 Laszlo Papp <lpapp@kde.org>
** Contact: http://www.qt.io/licensing/
**
** This file is part of the QtSerialPort module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL21$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file. Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** As a special exception, The Qt Company gives you certain additional
** rights. These rights are described in The Qt Company LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QApplication>
#include "mainwindow.h"
#include <QSplashScreen>
#include <QTimer>
#include "hmi.h"

#include "loginscreen.h"
#include "database.h"
#include "user.h"
#include "gpioclass.h"
#include "QProcess"



#define GUI_STARTUP_TIME    100

int main(int argc, char *argv[])
{
    PSOM* interfaceobj=nullptr;
    //    statemachine runstatemachine;


    QApplication a(argc, argv);

    MainWindow w;
    interfaceobj=w.getAdresseofObj();
    QPixmap pixmap(":/images/PSOM_Splashscreen/PSOM_Splashscreen.001.jpeg");
    QSplashScreen splash(pixmap);
    splash.show();

#ifdef Q_OS_LINUX
    splash.showMessage("Nils Minor ® 2017, running on Linux -> best System");
#else
    splash.showMessage("Nils Minor ® 2017, running on Sven & Steini & Mati & Flori Sven und steini waren nochnmal sven auch hier" + QSysInfo::prettyProductName() + " " + QSysInfo::currentCpuArchitecture());
#endif

    QTimer::singleShot(GUI_STARTUP_TIME, &splash, SLOT( close() ) );
#ifdef Q_OS_LINUX
    QTimer::singleShot(GUI_STARTUP_TIME, &w, SLOT( showMaximized() ) );
    w.rpiStartup( );
#else
    QTimer::singleShot(GUI_STARTUP_TIME, &w, SLOT( show() ) );
#endif

    //  GPIOClass* gpio26 = new GPIOClass("4");
    //   gpio26->setdir_gpio("out");
    //   gpio26->setval_gpio("0");
    QString p_stdout="default";
    QString p_stderr="default";


    QProcess *Process= new QProcess();
   // Process->start(help);


//    Process->start("bash", QStringList() << "-c" << "echo \"26\" > /sys/class/gpio/export");
//     Process->waitForFinished(30000);
//     Process->start("bash", QStringList() << "-c" << "echo \"out\" > /sys/class/gpio/gpio26/direction");
//      Process->waitForFinished(30000);
//      Process->start("bash", QStringList() << "-c" << "echo \"0\" > /sys/class/gpio/gpio26/value");
//       Process->waitForFinished(30000);



    hmi *HMI=new hmi(&w,interfaceobj);
    HMI->show();
    QObject::connect(&w,SIGNAL(hmishow()),HMI,SLOT(OpenHMI()));
    QObject::connect(HMI,SIGNAL(Lock()),&w,SLOT(enableLock()));
    QObject::connect(HMI,SIGNAL(UnLock()),&w,SLOT(disableLock()));



//    QObject::connect(HMI, SIGNAL(startLogging()),&w, SLOT(startcompressedlogging()));
//    QObject::connect(HMI, SIGNAL(stopLogging()),&w, SLOT(stopcompressedlogging()));
    QObject::connect(&w, SIGNAL(DataLoggerStarted(QString)),HMI,SLOT(SetDataLoggerPath(QString)));


    return a.exec();
}
