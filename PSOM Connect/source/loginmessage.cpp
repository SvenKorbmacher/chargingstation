#include "loginmessage.h"
#include "ui_loginmessage.h"

LoginMessage::LoginMessage(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LoginMessage)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint);
      this->setStyleSheet("background-color: transparent;");
   this->setAttribute(Qt::WA_TranslucentBackground);
     this->setFocusPolicy(Qt::NoFocus);
     ui->Message->setAlignment(Qt::AlignCenter);

}

LoginMessage::~LoginMessage()
{
    delete ui;
}

void LoginMessage::SetMessage(QString Message)
{
    ui->Message->setText(Message);

}
