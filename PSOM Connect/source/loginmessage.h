#ifndef LOGINMESSAGE_H
#define LOGINMESSAGE_H

#include <QWidget>

namespace Ui {
class LoginMessage;
}

class LoginMessage : public QWidget
{
    Q_OBJECT

public:
    explicit LoginMessage(QWidget *parent = 0);
    ~LoginMessage();
    void SetMessage(QString Message);

private:
    Ui::LoginMessage *ui;

};

#endif // LOGINMESSAGE_H
