#ifndef DATABASE_H
#define DATABASE_H


#include <qxmlstream.h>
#include <QFile>
#include <QtXml/QXmlReader>
#include <qmessagebox.h>
#include <QDebug>
#include "user.h"



class database
{
public:
    database();
   // bool FindUser_viaID(int id, int password, User* user);
    bool FindUser_viaID(QString id, QString password="default");


   bool FindUser_via_RFID(QString rfid);




   User* getUser();
   // bool FindUser_via_RFID(int RFID);


private:

    void CreateUser(QString id, QString password="default");

    QFile* xmlFile;
    QXmlStreamReader* xmlReader;
    bool m_validFile;
    User* m_user;
};

#endif // DATABASE_H
