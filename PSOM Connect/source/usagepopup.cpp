#include "usagepopup.h"
#include "ui_usagepopup.h"
#include <qdebug.h>
#include <QString>


UsagePopup::UsagePopup(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UsagePopup)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint);
    this->setStyleSheet("background-color: transparent;");
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setFocusPolicy(Qt::NoFocus);
}

UsagePopup::~UsagePopup()
{
    delete ui;
}

void UsagePopup::setValues(PSOM_measurement_data data)
{
    QString cutValue;


    cutValue.setNum(data.L1.voltage.rms,'f',3);
    ui->Voltage_L1->setText(cutValue);
    cutValue.setNum(data.L2.voltage.rms,'f',3);
    ui->Voltage_L2->setText(cutValue);
    cutValue.setNum(data.L3.voltage.rms,'f',3);
    ui->Voltage_L3->setText(cutValue);

    cutValue.setNum(data.L1.current.rms,'f',2);
    ui->Current_L1->setText(cutValue);
     cutValue.setNum(data.L2.current.rms,'f',2);
    ui->Current_L2->setText(cutValue);
     cutValue.setNum(data.L3.current.rms,'f',2);
    ui->Current_L3->setText(cutValue);

   cutValue.setNum(data.L1.power.active,'f',2);
    ui->Power_L1->setText(cutValue);
    cutValue.setNum(data.L2.power.active,'f',2);
    ui->Power_L2->setText(cutValue);
    cutValue.setNum(data.L3.power.active,'f',2);
    ui->Power_L3->setText(cutValue);

    cutValue.setNum(data.LT.energy.active,'f',2);
    ui->Power_Overall->setText(cutValue);
    ui->Charging_Time->setText(QString::number(data.evse.state));


    QDateTime time=time.currentDateTime();

    int secs = StartTime.msecsTo(time) / 1000;
    int mins = (secs / 60) % 60;
    int hours = (secs / 3600);
    secs = secs % 60;
    ui->Charging_Time->setText(QString("%1:%2:%3")
                               .arg(hours, 2, 10, QLatin1Char('0'))
                               .arg(mins, 2, 10, QLatin1Char('0'))
                               .arg(secs, 2, 10, QLatin1Char('0')) );

}

void UsagePopup::setStartTime(QDateTime time)
{
    ui->StartTime_2->setText(time.toString("ddd MMMM d yy hh:mm:ss"));
    StartTime=time;

}


