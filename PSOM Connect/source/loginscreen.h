#ifndef LOGINSCREEN_H
#define LOGINSCREEN_H


#include "numpad.h"
#include <QWidget>
#include "ui_loginscreen.h"
#include "mylineedit.h"
#include "database.h"
#include "user.h"
#include <QMovie>
#include <QProcess>
#include "loginmessage.h"
#include "hmipopup.h"
#include "gpioclass.h"


namespace Ui {
class LoginScreen;
}

class LoginScreen : public QWidget
{
    Q_OBJECT

public:
    explicit LoginScreen(QWidget *parent = 0);
    ~LoginScreen();
    void setdatabase(database* db);
//    void setGPIO(GPIOClass* gpio);

private slots:
    void on_LoginButton_1_clicked();

    void on_UserIDLineEdit_pressed(bool );

    void on_PasswordLineEdit_pressed(bool );

    void StartRfidAnimation();

    void StopRfidAnimation();

   // void on_RFID_Button_clicked();



    void on_NFCButton_clicked();

private:
    Ui::LoginScreen *ui;
    NumPad* np;
    database* m_database;
    User* m_user;
    QMovie *m_movie;
    QProcess* m_rfidProcess;
    QProcess* m_gpioProcess;
    bool m_NFCActive;

    LoginMessage* m_LoginMessage;
    hmipopup* PopUp;
 //   GPIOClass* m_GPIO;

    void WelcomeMessage();

    void ErrorMessage(QString Message);

    void EnableLoginGPIO();

    void BuildTouchField();

private slots:

    void CheckUser();

    void CloseWindow();

    void Error();


signals:

    void UnLock();


};

#endif // MAINWINDOW_H
