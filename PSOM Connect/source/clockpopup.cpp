#include "clockpopup.h"
#include "ui_clockpopup.h"


ClockPopup::ClockPopup(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ClockPopup)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint);

    this->setWindowFlags(Qt::FramelessWindowHint);
    this->setStyleSheet("background-color: transparent;");
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setFocusPolicy(Qt::NoFocus);

    m_Timer = new QTimer(this);
    connect(m_Timer, SIGNAL(timeout()), this, SLOT(updateLabels()));
    m_Timer->start(500);
      ui->AMP->setAlignment(Qt::AlignCenter);
     ui->DUTY->setAlignment(Qt::AlignCenter);
     ui->STATE->setAlignment(Qt::AlignCenter);



}

ClockPopup::~ClockPopup()
{
    delete ui;
}

void ClockPopup::setData(PSOM* InterfaceObject)
{
    m_InterfaceObject=InterfaceObject;
    pwm = new pwmFig();
    ui->layoutOscilloscope->addWidget(pwm->getScreenWidget());
    pwm->setInterface(InterfaceObject);

}

void ClockPopup::StopTimer()
{
    pwm->StopTimer();

}

void ClockPopup::updateLabels()
{
    QString help, help2;
    help.append("STATE: ");
    help2 = QString::number((m_InterfaceObject->getData()).evse.state);
    help.append(help2);
    ui->STATE->setText(help);

    help.clear();
    help.append("Duty: ");
    help2 = QString::number((m_InterfaceObject->getData()).evse.pwm_duty1);
    help.append(help2);
    ui->DUTY->setText(help);

    help.clear();
    help2.clear();
    help.append("Amp: ");
    help2.setNum((m_InterfaceObject->getData()).evse.adc1,'f',2);
    help.append(help2);
    ui->AMP->setText(help);

}

