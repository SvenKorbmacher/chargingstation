#ifndef MYLINEEDIT_H
#define MYLINEEDIT_H

#include <QLineEdit>

class MyLineEdit : public QLineEdit
{
  Q_OBJECT

public:
  MyLineEdit(QWidget *parent = 0);
  ~MyLineEdit();

signals:
  void pressed(bool pressed);

protected:
  virtual void mousePressEvent(QMouseEvent *e);

};

#endif
