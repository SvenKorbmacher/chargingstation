#include "user.h"
#include "QDebug"
#include "QDebug"


User::User()
{

    m_accessLevel="default";
    m_id="default";
    m_name="default";
    m_password="default";
    m_male="default";


}

QString User::getName()
{

    return m_name;
}

QString User::getAccesLevel(){

    return m_accessLevel;
}

QString User::getRFID(){

    return m_rfid;
}

QString User::getSex(){

    return m_male;
}

void User::SetAttributes(QString id, QString password, QString name, QString male, QString accesslevel)
{
    m_accessLevel=accesslevel;
    m_id=id;
    m_name=name;
    m_password=password;
    m_male=male;
}

QString User::getID(){

    return m_id;
}

