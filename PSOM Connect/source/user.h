#ifndef USER_H
#define USER_H

#include <QString>

class User
{
public:
    User();
    QString getName();
    QString getID();
    QString getPassword();
    QString getAccesLevel();
    QString getRFID();
    QString getSex();
    void SetAttributes(QString id, QString password, QString name, QString male, QString accesslevel);
    void clearAttributes();

private:
    QString m_name;
    QString m_id;
    QString m_password;
    QString m_accessLevel;
    QString m_rfid;
    QString m_male;


};

#endif // USER_H
