#ifndef USAGEPOPUP_H
#define USAGEPOPUP_H

#include <QWidget>
#include "psomQt.h"

namespace Ui {
class UsagePopup;
}

class UsagePopup : public QWidget
{
    Q_OBJECT

public:
    explicit UsagePopup(QWidget *parent = 0);
    ~UsagePopup();

public slots:
    void setValues(PSOM_measurement_data data);
    void setStartTime(QDateTime time);

private:
    Ui::UsagePopup *ui;
    QDateTime StartTime;
};

#endif // USAGEPOPUP_H
