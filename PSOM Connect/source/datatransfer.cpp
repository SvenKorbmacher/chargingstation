#include "datatransfer.h"
#include "ui_datatransfer.h"

DataTransfer::DataTransfer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DataTransfer)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint);
    this->setStyleSheet("background-color: transparent;");
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setFocusPolicy(Qt::NoFocus);

}

DataTransfer::~DataTransfer()
{
    delete ui;
}

void DataTransfer::setCheck(QString StorageName, QString FileName)
{
    QPixmap pixmap(":/images/Check.png");

    ui->State->setMaximumHeight(150);
    ui->State->setMaximumWidth(150);
    ui->State->setPixmap(pixmap);
    QString help="Device name:";
    ui->StorageName->setText(help.append(StorageName));
    ui->FileName->setText(FileName);



}

void DataTransfer::setUnchecked()
{
    QPixmap pixmap(":/images/Error.png");

    ui->State->setMaximumHeight(150);
    ui->State->setMaximumWidth(150);
    ui->State->setPixmap(pixmap);
    ui->StorageName->setText("No device detected");
    ui->FileName->setText("No file copied");


}


