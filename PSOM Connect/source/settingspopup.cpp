#include "settingspopup.h"
#include "ui_settingspopup.h"

SettingsPopup::SettingsPopup(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingsPopup)
{
    ui->setupUi(this);
       this->setWindowFlags(Qt::FramelessWindowHint);
    this->setWindowFlags(Qt::FramelessWindowHint);
    this->setStyleSheet("background-color: transparent;");
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setFocusPolicy(Qt::NoFocus);
    ui->ExpertMode->setFocusPolicy(Qt::NoFocus);
    ui->SafeToUsb->setFocusPolicy(Qt::NoFocus);

   // qApp->setStyleSheet ( " QTableWidget::item:focus { border: 0px }" );


    //ui->SafeToUsb->setAttribute();
}

SettingsPopup::~SettingsPopup()
{
    delete ui;
}


void SettingsPopup::on_ExpertMode_released()
{

    emit ShowExpertMode();

}



void SettingsPopup::on_SafeToUsb_released()
{
    emit SafeToUsb();
}
