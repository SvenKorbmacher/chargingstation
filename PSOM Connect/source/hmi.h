#ifndef HMI_H
#define HMI_H


#include <QWidget>
#include <QTime>
#include <QDialog>
#include "hmipopup.h"
#include "usagepopup.h"
#include "settingspopup.h"
#include "clockpopup.h"
#include "userpopup.h"
#include "psomQt.h"
#include "loginscreen.h"
#include "interface.h"
#include "mainwindow.h"
#include "QTime"
#include "user.h"
#include "database.h"
#include "mData/mdatalogger.h"
#include <QFile>
#include "datatransfer.h"
#include "gpioclass.h"



namespace Ui {
class hmi;
}

class hmi : public QWidget
{
    Q_OBJECT

public:
    explicit hmi(QWidget *parent, PSOM* interfaceObj);


    ~hmi();
private slots:

    void CloseWindow();
    void on_IdButton_clicked();
    void on_UsageButton_clicked();
    void on_ClockButton_clicked();
    void on_ConfigButton_clicked();
    void ShowExpert();
    void ShowLoginScreen();
    void safeToUsb();
    void UpdateHMI(PSOM_measurement_data data);
    void SetDataLoggerPath(QString filename);

private:
    Ui::hmi *ui;
    QMovie* movie;
    QTime StartTime;
    QDateTime m_StartTime;
    hmipopup* PopUp;
    QTimer* DialogUpdateTimer;
    UsagePopup* UsageDialog;
    SettingsPopup* SettingsDialog;
    ClockPopup* ClockDialog;
    UserPopup* UserDialog;
    PSOM_measurement_data m_data;
    PSOM* m_Interface;
    QWidget* m_RefFrame;
    LoginScreen* m_login;
    User* m_user;
    database* m_database;
    mDataLogger* m_datalogger;
    QFile m_file;
    QString m_filename;
    QProcess *m_GPIOProcess;
    unsigned char m_State;
    void BuildTouchField();
    void IDLE();
    void ERROR();
    void CHARGING();
    void DisalbeGPIO();


public slots:
    void OpenHMI();
    void setStartTime(QDateTime time);
    void Unlock();


signals:
    void Lock();
void UnLock();

};

#endif // HMI_H
