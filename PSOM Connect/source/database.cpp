#include "database.h"
#include "qdir.h"


database::database()
{
    m_user=new User();
    xmlFile = new QFile();
   // xmlFile->setFileName("/home/chargingstation/chargingstation/PSOM Connect/source/database.xml");
   xmlFile->setFileName("/home/pi/database.xml"); // for pi

    if (false==xmlFile->exists()) {
        //  qDebug() <<"No Database File exists";
    }
    else{

        m_validFile=true;
        //  qDebug() << "valid file"<<m_validFile;

    }

}

bool database::FindUser_viaID(QString id, QString password)
{

//    if(m_user!=nullptr)
//        delete m_user;

    bool userfound=false;
    QString readid="default";
    QString readpassword="default";

    if(m_validFile){
        //Open xml file for reading

        xmlFile->open((QIODevice::ReadOnly));
        xmlReader = new QXmlStreamReader(xmlFile);


        //Parse the XML until we reach end of it
        while(!xmlReader->atEnd() && !xmlReader->hasError()) {
            // Read next element
            QXmlStreamReader::TokenType token = xmlReader->readNext();
            //If token is just StartDocument - go to next
            if(token == QXmlStreamReader::StartDocument) {
                continue;
            }
            //If token is StartElement - read it
            if(token == QXmlStreamReader::StartElement) {

                if(xmlReader->name() == "accounts")
                    continue;

                if(xmlReader->name() == "user")
                    continue;

                if(xmlReader->name() == "pinid") {
                    readid = xmlReader->readElementText();
                    continue;
                }
                if(xmlReader->name() == "details")
                    continue;

                if(xmlReader->name() == "password") {
                    readpassword= xmlReader->readElementText();
                    if(readid==id && readpassword==password){
                        CreateUser(readid, readpassword);
                        //valid user found in database
                        userfound=true;
                        break;
                    }
                    else{
                        continue;
                    }

                }
            }
            else{
                //no start element found
            }
        }

        //close reader and flush file
        xmlReader->clear();
        xmlFile->close();

        if(userfound==true){
            return true;
        }
    }
    else{
        qDebug() << "No Valid file";
    }
    return false;
}

bool database::FindUser_via_RFID(QString rfid)
{

//    if(m_user!=nullptr)
//        delete m_user;

    QString readrfid="default";
    QString readid="default";
    QString readpassword="default";
        bool statement=false;

    if(m_validFile){
        //Open xml file for reading
        xmlFile->open((QIODevice::ReadOnly));
        xmlReader = new QXmlStreamReader(xmlFile);

        //Parse the XML until we reach end of it
        while(!xmlReader->atEnd() && !xmlReader->hasError()) {
            // Read next element
            QXmlStreamReader::TokenType token = xmlReader->readNext();
            //If token is just StartDocument - go to next
            if(token == QXmlStreamReader::StartDocument) {
                continue;
            }
            //If token is StartElement - read it
            if(token == QXmlStreamReader::StartElement) {

                if(xmlReader->name() == "accounts")
                    continue;

                if(xmlReader->name() == "nfc")
                    continue;

                if(xmlReader->name() == "rfid") {
                    readrfid = xmlReader->readElementText();

                    continue;
                }
                if(xmlReader->name() == "carddetails")
                    continue;

                if(xmlReader->name() == "id"){
                     readid= xmlReader->readElementText();

                    continue;
                }

                if(xmlReader->name() == "password") {
                    readpassword= xmlReader->readElementText();

                    if(readrfid==rfid){
                        xmlReader->clear();
                        xmlFile->close();


                       statement=FindUser_viaID(readid, readpassword);
                        //valid user found in database

                        break;

                    }
                    else {
                        continue;
                    }

                }
            }
            else{
                //no start element found
            }
        } //end while

        //close reader and flush file

    }
    else{
        qDebug() << "No Valid file";
    }
    xmlReader->clear();
    xmlFile->close();
    return statement;

}

User *database::getUser()
{

    return m_user;

}








void database::CreateUser(QString id, QString password)
{
    QString readAcces="default";
    QString readMale="default";
    QString readName="default";

    while(!xmlReader->atEnd() && !xmlReader->hasError()) {
        // Read next element
        xmlReader->readNext();

        if(xmlReader->name() == "acceslevel") {
            readAcces=xmlReader->readElementText();
            continue;
        }
        if(xmlReader->name() == "sex") {
            readMale=xmlReader->readElementText();
            continue;
        }
        if(xmlReader->name() == "name") {
            readName = xmlReader->readElementText();
            continue;
        }
        if(xmlReader->name() == "pinid"){
            break;
        }
    } //while end

    m_user->SetAttributes(id, password, readName, readMale, readAcces);

}
