#ifndef CLOCKPOPUP_H
#define CLOCKPOPUP_H

#include <QWidget>
#include "qOsci/qosci.h"
#include "pwmfig.h"
#include "interface.h"
#include "psomQt.h"

namespace Ui {
class ClockPopup;
}

class ClockPopup : public QWidget
{
    Q_OBJECT

public:
    explicit ClockPopup(QWidget *parent = 0);
    ~ClockPopup();
    void setData(PSOM* InterfaceObject);
    void StopTimer();

private:
    Ui::ClockPopup *ui;
    pwmFig                      *pwm;
    PSOM_measurement_data m_data;
    PSOM* m_InterfaceObject;
    QTimer *m_Timer;

private slots:

    void updateLabels();

};

#endif // CLOCKPOPUP_H
