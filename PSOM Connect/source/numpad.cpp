#include "numpad.h"

NumPad::NumPad(QWidget *parent) : QDialog(parent)
{
    if (this->objectName().isEmpty())
        this->setObjectName(QString::fromUtf8("NumPad"));
    this->resize(270, 300);

    this->setStyleSheet("QPushButton{background-color:rgb(160,160,160);"
                        "border-style: outset;"
                        "border-width: 2px;"
                        "border-radius: 10px;"
                        "border-color:gray}"

                        "QPushButton:pressed{background-color:rgb(160,160,160);"
                        "border-style: outset;"
                        "border-width: 2px;"
                        "border-radius: 10px;"
                        "border-color:rgb(102,108,255)}"

                        "QPushButton:focus {outline:0}"

                        "QLineEdit{background-color:white}"

                        "QDialog{background-color:rgb(192,192,192);"
                        "border-style:outset;"
                        "border-width: 2px;"
                        "border-radius: 10px;"
                        "border-color:gray}");



    m_0       = new QPushButton(this);
    m_1       = new QPushButton(this);
    m_2       = new QPushButton(this);
    m_3       = new QPushButton(this);
    m_4       = new QPushButton(this);
    m_5       = new QPushButton(this);
    m_6       = new QPushButton(this);
    m_7       = new QPushButton(this);
    m_8       = new QPushButton(this);
    m_9       = new QPushButton(this);
    m_dot     = new QPushButton(this);
    m_ok      = new QPushButton(this);
    m_esc     = new QPushButton(this);
    m_display = new QLineEdit  (this);

    m_0->setObjectName      (QString::fromUtf8("m_0"));
    m_1->setObjectName      (QString::fromUtf8("m_1"));
    m_2->setObjectName      (QString::fromUtf8("m_2"));
    m_3->setObjectName      (QString::fromUtf8("m_3"));
    m_4->setObjectName      (QString::fromUtf8("m_4"));
    m_5->setObjectName      (QString::fromUtf8("m_5"));
    m_6->setObjectName      (QString::fromUtf8("m_6"));
    m_7->setObjectName      (QString::fromUtf8("m_7"));
    m_8->setObjectName      (QString::fromUtf8("m_8"));
    m_9->setObjectName      (QString::fromUtf8("m_9"));
    m_ok->setObjectName     (QString::fromUtf8("m_ok"));
    m_dot->setObjectName    (QString::fromUtf8("m_dot"));
    m_esc->setObjectName    (QString::fromUtf8("m_esc"));
    m_display->setObjectName(QString::fromUtf8("m_display"));

    m_0->setGeometry(      QRect(20 , 240, 111, 51 ));
    m_1->setGeometry(      QRect(20 , 180, 51 , 51 ));
    m_2->setGeometry(      QRect(80 , 180, 51 , 51 ));
    m_3->setGeometry(      QRect(140, 180, 51 , 51 ));
    m_4->setGeometry(      QRect(20 , 120, 51 , 51 ));
    m_5->setGeometry(      QRect(80 , 120, 51 , 51 ));
    m_6->setGeometry(      QRect(140, 120, 51 , 51 ));
    m_7->setGeometry(      QRect(20 , 60 , 51 , 51 ));
    m_8->setGeometry(      QRect(80 , 60 , 51 , 51 ));
    m_9->setGeometry(      QRect(140, 60 , 51 , 51 ));
    m_ok->setGeometry(     QRect(200, 180, 51 , 111));
    m_dot->setGeometry(    QRect(140, 240, 51 , 51 ));
    m_esc->setGeometry(    QRect(200, 60 , 51 , 111));
    m_display->setGeometry(QRect(20 , 10 , 231, 41 ));

    retranslateUi(this);

    QMetaObject::connectSlotsByName(this);

}

void NumPad::retranslateUi(QDialog *n)
{
    n->setWindowTitle(QApplication::translate("NumPad", "Dialog", 0));
    m_7->setText(QApplication::translate("NumPad", "7", 0));
    m_4->setText(QApplication::translate("NumPad", "4", 0));
    m_1->setText(QApplication::translate("NumPad", "1", 0));
    m_0->setText(QApplication::translate("NumPad", "0", 0));
    m_8->setText(QApplication::translate("NumPad", "8", 0));
    m_9->setText(QApplication::translate("NumPad", "9", 0));
    m_5->setText(QApplication::translate("NumPad", "5", 0));
    m_6->setText(QApplication::translate("NumPad", "6", 0));
    m_2->setText(QApplication::translate("NumPad", "2", 0));
    m_3->setText(QApplication::translate("NumPad", "3", 0));
    m_dot->setText(QApplication::translate("NumPad", ".", 0));
    m_ok->setText( QApplication::translate("NumPad", "OK", 0));
    m_esc->setText(QApplication::translate("NumPad", "ESC", 0));
}


