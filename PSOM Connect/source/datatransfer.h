#ifndef DATATRANSFER_H
#define DATATRANSFER_H

#include <QWidget>

namespace Ui {
class DataTransfer;
}

class DataTransfer : public QWidget
{
    Q_OBJECT

public:
    explicit DataTransfer(QWidget *parent = 0);
    ~DataTransfer();
    void setCheck(QString StorageName, QString FileName);
    void setUnchecked();



private:
    Ui::DataTransfer *ui;

};

#endif // DATATRANSFER_H
