#include "userpopup.h"
#include "ui_userpopup.h"

UserPopup::UserPopup(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UserPopup)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint);
      this->setStyleSheet("background-color: transparent;");
   this->setAttribute(Qt::WA_TranslucentBackground);
     this->setFocusPolicy(Qt::NoFocus);
    ui->UserLogut->setFocusPolicy(Qt::NoFocus);



    //  setWindowFlags(Qt::FramelessWindowHint);@
}

void UserPopup::UpdateLabels(QString name, QString accessLevel, QString id)
{

    ui->Acces->setText(accessLevel);
    ui->Name->setText(name);
    ui->UserID->setText(id);

}

//void UserPopup::setLogin(LoginScreen *Login)
//{
//    m_Login=Login;
//}

UserPopup::~UserPopup()
{
    delete ui;
}


void UserPopup::on_UserLogut_released()
{
    emit ShowLogin();
}
