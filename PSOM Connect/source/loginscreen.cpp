#include "loginscreen.h"
#include "ui_loginscreen.h"
#include "numpad.h"
#include "QDialog"
#include "QTextEdit"
#include "QPushButton"
#include "QVBoxLayout"
#include "QTextBlock"
#include "QLabel"
#include "QPalette"
#include <QProcess>
#include "QThread"


LoginScreen::LoginScreen(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LoginScreen)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint);
    QPalette pal;
    pal.setColor(QPalette::Background, Qt::white);
    this->setPalette(pal);

    QPixmap pixmap(":/images/Universal_Contactless_Card_Symbol.svg_klein.png");
    ui->Animation->setMaximumHeight(236);
    ui->Animation->setMaximumWidth(400);
    ui->Animation->setPixmap(pixmap);
    ui->NFCButton->setFlat(true);
    ui->NFCButton->setFocusPolicy(Qt::NoFocus);
    ui->NFCButton->setStyleSheet("background-color: transparent;");
    m_LoginMessage=nullptr;
    m_NFCActive=false;


}

LoginScreen::~LoginScreen()
{
    delete ui;
}

void LoginScreen::setdatabase(database *db)
{
    m_database=db;
}

void LoginScreen::on_LoginButton_1_clicked()
{
    if(m_NFCActive==false){

        QString username,password;
        username = ui->UserIDLineEdit->text();
        password = ui->PasswordLineEdit->text();

        if(m_database->FindUser_viaID(username, password)){
            this->hide();
            EnableLoginGPIO();
            ui->PasswordLineEdit->clear();
            ui->UserIDLineEdit->clear();
            m_user=m_database->getUser();
            WelcomeMessage();
        }
        else{
            ui->PasswordLineEdit->clear();
            ui->UserIDLineEdit->clear();
            ErrorMessage("Invalid Username or Pin - Please try again!");

        }


    }

}


void LoginScreen::on_UserIDLineEdit_pressed(bool )
{
    if(m_NFCActive==false){
    np = new NumPad(this);
    if(np->exec()){
this->activateWindow();
        ui->UserIDLineEdit->setText(np->getString());
    };
    }
}

void LoginScreen::on_PasswordLineEdit_pressed(bool )
{
    if(m_NFCActive==false){
    np = new NumPad(this);
    if(np->exec()){

        ui->PasswordLineEdit->setText(np->getString());
    };
    }
}

void LoginScreen::StartRfidAnimation()
{
    m_movie = new QMovie(":/images/Universal_Contactless_Card_Symbol.svg.gif");
    ui->Animation->clear();
    ui->Animation->setMaximumHeight(236);
    ui->Animation->setMinimumWidth(400);
    ui->Animation->setMovie(m_movie);
    m_movie->setSpeed(100);
    m_movie->start();
    QFont f= ui->Animation->font();
    f.setStyleStrategy(QFont::PreferAntialias);
    ui->Animation->setFont(f);
    ui->Animation->update();
}

void LoginScreen::StopRfidAnimation()
{
    m_movie->stop();
    delete m_movie;
    QPixmap pixmap(":/images/Universal_Contactless_Card_Symbol.svg_klein.png");
    ui->Animation->setPixmap(pixmap);
}


void LoginScreen::on_NFCButton_clicked()
{
    if(m_NFCActive==false){
        m_NFCActive=true;
        StartRfidAnimation();
        m_rfidProcess= new QProcess();
        m_rfidProcess->start("nfc-poll");
        connect(m_rfidProcess, SIGNAL(finished(int , QProcess::ExitStatus )), this, SLOT(CheckUser()));
        connect(m_rfidProcess, SIGNAL(errorOccurred(QProcess::ProcessError)), this, SLOT(Error()));
    }

    else {

        // do nothing

    }

}



void LoginScreen::EnableLoginGPIO()
{

    emit UnLock();
//    qDebug() <<"EnableLogging GPIO invoked";
//    m_gpioProcess= new QProcess();
//    m_gpioProcess->start("bash", QStringList() << "-c" << "echo \"1\" > /sys/class/gpio/gpio26/value");
//    m_gpioProcess->waitForFinished(300);
//    m_gpioProcess->kill();
//    delete m_gpioProcess;

}

void LoginScreen::BuildTouchField()
{
    PopUp= new hmipopup();
    QHBoxLayout *layout = new QHBoxLayout();
    QPushButton *button1 = new QPushButton();
    button1->setFixedHeight(480);
    button1->setFixedWidth(800);
    button1->setFlat(true);
    button1->setFocusPolicy(Qt::NoFocus);
    button1->setStyleSheet("background-color: transparent;");
    layout->addWidget(button1);
    PopUp->setLayout(layout);
    PopUp->setAttribute(Qt::WA_TranslucentBackground);
    PopUp->setWindowFlags(Qt::FramelessWindowHint);
    PopUp->show();
    PopUp->move(0,0);
    connect(button1,SIGNAL(clicked()),this,SLOT(CloseWindow()));
}

void LoginScreen::CheckUser()
{
    m_NFCActive=false;
    StopRfidAnimation();
    QString p_stdout="default";
    QString p_stderr="default";
    p_stdout = m_rfidProcess->readAllStandardOutput();
    p_stderr = m_rfidProcess->readAllStandardError();
    QString substring=(p_stdout.right(p_stdout.length()- p_stdout.indexOf("NFCID1")-8));
    QString nfcid=substring.left(substring.indexOf("\n"));
    qDebug() << nfcid;
    if(m_database->FindUser_via_RFID(nfcid)){
        this->hide();
        EnableLoginGPIO();
        m_user=m_database->getUser();
        WelcomeMessage();
     }
    else{
        ErrorMessage("NO RFID-Card detected or not registered");
    }
}



void LoginScreen::Error()
{
    m_NFCActive=false;
    StopRfidAnimation();
    ErrorMessage("Error while reading RFID-Card");
}

void LoginScreen::WelcomeMessage()
{

    BuildTouchField();
    m_LoginMessage = new LoginMessage();

    QString tmp;
    tmp.append("Welcome");
    if(m_user->getSex()=="male")
        tmp.append(" Mr. ");

    if(m_user->getSex()=="female")
        tmp.append(" Ms. ");

    tmp.append(m_user->getName());
    m_LoginMessage->SetMessage(tmp);
    m_LoginMessage->move(145,115);

    m_LoginMessage->show();


}

void LoginScreen::ErrorMessage(QString Message)
{
    BuildTouchField();
    m_LoginMessage = new LoginMessage();
    m_LoginMessage->SetMessage(Message);
    m_LoginMessage->move(145,115);
    m_LoginMessage->show();
}

void LoginScreen::CloseWindow()
{
    PopUp->close();
    delete PopUp;
    PopUp=nullptr;



    if(m_LoginMessage != nullptr){

        m_LoginMessage->close();
        delete m_LoginMessage;
        m_LoginMessage=nullptr;
    }

    if(m_NFCActive==true){

        m_rfidProcess->kill();
        delete m_rfidProcess;
    }



}
