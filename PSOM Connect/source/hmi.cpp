#include "hmi.h"
#include "ui_hmi.h"

#include <QGraphicsView>

#include <QGraphicsProxyWidget>
#include <QLabel>
#include <QMovie>
#include <QTimer>
#include <QString>
#include <QTime>
#include <QHBoxLayout>
#include "qdebug.h"
#include <QWidget>
#include <QFileInfo>
#include <QIODevice>
#include  <QList>


hmi::hmi(QWidget *parent, PSOM* interfaceObj) :
    QWidget(parent),
    ui(new Ui::hmi)
{
    ui->setupUi(this);
    QPalette pal;
    pal.setColor(QPalette::Background, Qt::white);
    this->setPalette(pal);
    this->setWindowFlags(Qt::FramelessWindowHint);
    ui->State_Info_Text->setAlignment(Qt::AlignCenter);
    ui->STATE->setAlignment(Qt::AlignCenter);

    m_database= new database();
    m_user=m_database->getUser();
    m_State=0;
    PopUp=nullptr;
    UsageDialog=nullptr;
    SettingsDialog= nullptr;
    ClockDialog=nullptr;
    UserDialog=nullptr;
    m_login=new LoginScreen();
    m_login->setdatabase(m_database);
    m_RefFrame=parent;
    m_Interface=interfaceObj;
    m_StartTime=(QDateTime::currentDateTime());

    IDLE();
    connect(m_Interface,SIGNAL(sendTimestamptoHMI(QDateTime)), this, SLOT(setStartTime(QDateTime)));
    connect(m_Interface,SIGNAL(updatePSOMdata(PSOM_measurement_data)), this, SLOT(UpdateHMI(PSOM_measurement_data)));
    connect(m_login, SIGNAL(UnLock()), this, SLOT(Unlock()));
    QTimer::singleShot(100, m_login, SLOT( show() ) );
}

hmi::~hmi()
{
    delete ui;
}
/* closes all dialogs when the singal from BuildTouchField is is thrown.
 */
void hmi::CloseWindow()
{
    PopUp->close();
    delete PopUp;
    PopUp=nullptr;

    if(UsageDialog != nullptr){

        UsageDialog->close();
        delete UsageDialog;
        UsageDialog=nullptr;
    }
    if(SettingsDialog != nullptr){

        SettingsDialog->close();
        delete SettingsDialog;
        SettingsDialog=nullptr;

    }
    if(ClockDialog != nullptr){
        ClockDialog->StopTimer();
        ClockDialog->close();
        delete ClockDialog;
        ClockDialog=nullptr;
    }
    if(UserDialog != nullptr){

        UserDialog->close();
        delete UserDialog;
        UserDialog=nullptr;
    }


}

/*
 * BuildTouchField is needed for creating an invisible button behind the open dialog.
 * When a Signal is fired the open dialog is closed and deleted.
 */
void hmi::BuildTouchField(){

    PopUp= new hmipopup();
    QHBoxLayout *layout = new QHBoxLayout();
    QPushButton *button1 = new QPushButton();
    button1->setFixedHeight(480);
    button1->setFixedWidth(800);
    button1->setFlat(true);
    button1->setFocusPolicy(Qt::NoFocus);
    button1->setStyleSheet("background-color: transparent;");
    layout->addWidget(button1);
    PopUp->setLayout(layout);
    PopUp->setAttribute(Qt::WA_TranslucentBackground);
    PopUp->setWindowFlags(Qt::FramelessWindowHint);
    PopUp->show();
    PopUp->move(0,0);
    connect(button1,SIGNAL(clicked()),this,SLOT(CloseWindow()));

}

/*
 * IDLE() updates the HMI and load the Idle_Big.gif
 */
void hmi::IDLE()
{
    movie = new QMovie(":/images/Idle_Big.gif");
    ui->label->setMovie(movie);
    movie->setSpeed(800);
    movie->start();
    QFont f= ui->label->font();
    f.setStyleStrategy(QFont::PreferAntialias);
    ui->label->setFont(f);

}

/*
 * IDLE() updates the HMI and load the Error_Big.gif
 */
void hmi::ERROR()
{
    delete movie;

    movie = new QMovie(":/images/Error_Big.gif");
    ui->label->setMovie(movie);
    movie->setSpeed(2000);
    movie->start();
    QFont f= ui->label->font();
    f.setStyleStrategy(QFont::PreferAntialias);
    ui->label->setFont(f);

}
/*
 *Charging updates the HMI and load the Loading_Big.gif
 */
void hmi::CHARGING()
{
    m_StartTime=(QDateTime::currentDateTime());
    delete movie;
    StartTime.start();

    movie = new QMovie(":/images/Loading_Big.gif");
    movie->setSpeed(800);
    ui->label->setMovie(movie);
    movie->start();
    QFont f= ui->label->font();
    f.setStyleStrategy(QFont::PreferAntialias);
    ui->label->setFont(f);
}

/*
 * Starts a Process and set the GPIO PIN 26 to 0V
 */
void hmi::DisalbeGPIO()
{

    emit Lock();
//    m_GPIOProcess=new QProcess();
//    m_GPIOProcess->start("bash", QStringList() << "-c" << "echo \"0\" > /sys/class/gpio/gpio26/value");
//    m_GPIOProcess->waitForFinished(300);
//    m_GPIOProcess->kill();
//    delete m_GPIOProcess;

}

/*
 * Set the StartTime when the PSOM board provided a vaule of 2 or 3 for the state
 */
void hmi::setStartTime(QDateTime time)
{
    m_StartTime=time;
}

void hmi::Unlock()
{
    emit UnLock();
}

/*
 * Hides the HMI and shows the GUI from the PSOM Board
 */
void hmi::ShowExpert()
{
    if(m_user->getAccesLevel()=="expert"){
        this->hide();
        SettingsDialog->hide();
        m_RefFrame->show();
        m_RefFrame->activateWindow();}
    else{
        //do nothing
    }
}

/*
 * Log out from HMI
 * The GPIO Pin is set to 0V
 * The Log file is deleted
 * The Login screen is shown
 *  */
void hmi::ShowLoginScreen()
{
    DisalbeGPIO();
    QString filePath="/home/pi/";
    filePath.append(m_filename);
    if (QFile::exists(filePath)) QFile::remove(filePath);
    UserDialog->hide();
    m_login->show();
    m_login->activateWindow();
}

/*
 * Transfer the Log file to the USB device
 *
 */
void hmi::safeToUsb()
{
    CloseWindow();
    PopUp= new hmipopup();
    QHBoxLayout *layout = new QHBoxLayout();
    QPushButton *button1 = new QPushButton();
    button1->setFixedHeight(480);
    button1->setFixedWidth(800);
    button1->setFlat(true);
    button1->setFocusPolicy(Qt::NoFocus);
    button1->setStyleSheet("background-color: transparent;");
    layout->addWidget(button1);
    PopUp->setLayout(layout);
    PopUp->setAttribute(Qt::WA_TranslucentBackground);
    PopUp->setWindowFlags(Qt::FramelessWindowHint);
    PopUp->show();
    PopUp->move(0,0);

    DataTransfer* Message= new DataTransfer();
    Message->move(145,115);
    Message->show();
    Message->activateWindow();

    QString location;
    bool succes=false;
    QString StorageName="default";
    QString FileName="Default";
    QString transfer="Default";





    qDebug() << m_filename;
    foreach (const QStorageInfo &storage, QStorageInfo::mountedVolumes()) {
        if (storage.isValid() && storage.isReady() && (!(storage.name()).isEmpty())&& (!(storage.name()=="boot"))) {
            if (!storage.isReadOnly()) {

                //WILL CREATE A FILE IN A BUILD FOLDER
                location = storage.rootPath();
                QString filePath="/home/pi/";
                filePath.append(m_filename);



                QString srcPath = filePath;
                //PATH OF THE FOLDER IN PENDRIVE
                location="/media/usb0";
                QString destPath = location;
                destPath.append("/");
                destPath.append(m_filename);
                QFile file(srcPath); //create file from logged data stream

             //   if(file.exists()){  //if logfile exists start copy procedure

               //     if (QFile::exists(destPath)) QFile::remove(destPath); //if file is already on drive -> delete it
                  //  if (file.size() < storage.bytesFree()) {
//                        QFile::copy(srcPath,destPath);
                     //   transfer="cp /home/pi/";
                        transfer="cp ";
                        transfer.append(m_filename);
                        transfer.append(" /media/usb0");
                        qDebug() <<"QString" <<transfer;
                        QByteArray ba=transfer.toLatin1();
                        const char *commandline=ba.data();
                        system(commandline);

                    //    }

                    // check if transfer was sucessfulll
                    QDirIterator dirIt(location, QDirIterator::Subdirectories);
                    while (dirIt.hasNext()) {
                        dirIt.next();
                        if (QFileInfo(dirIt.filePath()).isFile()) {
                            if (QFileInfo(dirIt.filePath()).fileName() == m_filename){
                                succes=true;
                                StorageName= storage.name();
                                FileName=m_filename;


                            }

                        }
                    }

              //  }

            }
        }

    }

    if(succes==true){

        Message->setCheck(StorageName, FileName);
        Message->update();
    }
    else{

        Message->setUnchecked();
        Message->update();

    }

    connect(button1,SIGNAL(clicked()),Message,SLOT(close()));
    connect(button1,SIGNAL(clicked()),this,SLOT(CloseWindow()));


}

/*
 * State machine for shwoing the right GIF based on the Provided state value
 */
void hmi::UpdateHMI(PSOM_measurement_data data)
{    
    switch(data.evse.state){

    case 0:
        if(m_State!=0){

            ui->STATE->setText("STATE: A ");
            ui->State_Info_Text->setText("NO EV Connected");
            IDLE();
            m_State=0;}
        break;
    case 1:

        if(m_State!=1){
            ui->STATE->setText("STATE: B");
            ui->State_Info_Text->setText("EV connected, not ready to charge");
            IDLE();
            m_State=1;}
        break;

    case 2:

        if(m_State!=2){
            ui->STATE->setText("STATE: C");
            ui->State_Info_Text->setText("Charging, without ventilation");
            CHARGING();
            m_State=2;}
        break;

    case 3:
        if(m_State!=3){

            ui->STATE->setText("STATE: D");// + QString::number(data.evse.state));
            ui->State_Info_Text->setText("Charging, with ventilation");
            CHARGING();
            m_State=3;}
        break;

    case 4:
        if(m_State!='4'){
            ui->STATE->setText("STATE: E");// + QString::number(data.evse.state));
            ui->State_Info_Text->setText("Error");
            ERROR();
            m_State=4;}
        break;

    case 5:
        if(m_State!=5){
            ui->STATE->setText("STATE: F");// + QString::number(data.evse.state));
            ui->State_Info_Text->setText("Error");
            ERROR();
            m_State=5;}
        break;

    case 6:
        if(m_State!=6){
            ui->STATE->setText("STATE: Locked");// + QString::number(data.evse.state));
            ui->State_Info_Text->setText("No INFO");
            ERROR();
            m_State=6;}
        break;
    default:
        if(m_State!=100){
            ui->STATE->setText("STATE: 0");
            ui->State_Info_Text->setText("Fatal Error! Check Hw.");
            ERROR();
            m_State=100;}
        break;
    }
}
/*
 * Is invoked by a SIGNAL provided by the datalogger to get the File name
 */
void hmi::SetDataLoggerPath(QString filename)
{
    m_filename=filename;
}
/*
 * SLOT is invoked by the Loginscreen to show the HMI
 */
void hmi::OpenHMI()
{
    this->show();
}


void hmi::on_IdButton_clicked()
{
    if(UserDialog==nullptr && PopUp==nullptr){

        //build invisible close field behind the dialog
        BuildTouchField();
        //Build Dialog for Usage informations
        UserDialog=new UserPopup();
        UserDialog->show();
        UserDialog->move(145,115);
        UserDialog->UpdateLabels(m_user->getName(), m_user->getAccesLevel(), m_user->getID());
        connect(UserDialog,SIGNAL(ShowLogin()),this, SLOT(ShowLoginScreen()));
    }
    else{
        // do nothing if the button is pushed again and the dialog is shown
    }
}


void hmi::on_UsageButton_clicked()
{
    if(UsageDialog==nullptr && PopUp==nullptr){

        //build invisible close field behind the dialog
        BuildTouchField();
        //Build Dialog for Usage informations
        UsageDialog=new UsagePopup();
        UsageDialog->show();
        UsageDialog->move(145,115);

        connect(m_Interface,SIGNAL(updatePSOMdata(PSOM_measurement_data)), UsageDialog, SLOT(setValues(PSOM_measurement_data)));
        UsageDialog->setStartTime(m_StartTime);
     }
    else{
        // do nothing if the button is pushed again and the dialog is shown
    }

}

/*
 * Clock Button is now the diagramm button
 */
void hmi::on_ClockButton_clicked()
{
    if(UsageDialog==nullptr && PopUp==nullptr){

        //build invisible close field behind the dialog
        BuildTouchField();

        //Build Dialog for Usage informations
        ClockDialog=new ClockPopup();
        ClockDialog->setData(m_Interface);
        ClockDialog->show();
        ClockDialog->move(145,115);

    }
    else{
        // do nothing if the button is pushed again and the dialog is shown
    }

}

void hmi::on_ConfigButton_clicked()
{

       if(SettingsDialog==nullptr && PopUp==nullptr){

        //build invisible close field behind the dialog
        BuildTouchField();
        //Build Dialog for Usage informations
        SettingsDialog=new SettingsPopup();
        SettingsDialog->show();
        SettingsDialog->move(145,115);
        connect(SettingsDialog,SIGNAL(ShowExpertMode()),this, SLOT(ShowExpert()));
        connect(SettingsDialog,SIGNAL(SafeToUsb()),this, SLOT(safeToUsb()));

    }
    else{
        // do nothing if the button is pushed again and the dialog is shown
    }
}



