#ifndef INTERFACE_H
#define INTERFACE_H
#include "psomQt.h"
#include <stdint.h>




//#define HARM_QUANTITY				11

//enum HarmonicType2 {
//    VoltageHarmonics2 = 1,
//    CurrentHarmonics2 = 2,
//    PPowerHarmonics2 = 3,
//    QPowerHarmonics2 = 4
//};

//enum ReadingStates2 {
//    readNormal2 = 0,
//    readCalData2 = 1
//};

//struct PSOM_Voltage2 {
//    float rms;
//    float thd;
//    float peak;
//    float fundamental;
//    float harmonic;
//};
//struct PSOM_Current2{
//    float rms;
//    float thd;
//    float peak;
//    float fundamental;
//    float harmonic;
//};
//struct PSOM_Power2 {
//    float active;
//    float reactive;
//    float apparent;
//    float factor;
//    float cos_phi;
//};
//struct PSOM_Energy2 {
//    float active;
//    float reactive;
//    float cost;
//};
//struct PSOM_Harmonics2 {
//    float contentL1 [HARM_QUANTITY] = {0};
//    float contentL2 [HARM_QUANTITY] = {0};
//    float contentL3 [HARM_QUANTITY] = {0};
//};

//struct PSOM_phases2 {
//    PSOM_Voltage2 voltage;
//    PSOM_Current2 current;
//    PSOM_Power2  power;
//    PSOM_Energy2 energy;
//    PSOM_Harmonics2 harmonic;
//};
//struct EVSE2 {
//    float adc1;
//    float adc2;
//    float adc3;
//    float adc4;

//    int pwm_duty1;
//    int pwm_duty2;
//    int pwm_duty3;
//};

//struct PSOM_measurement_datas {
//    PSOM_phases2 L1;
//    PSOM_phases2 L2;
//    PSOM_phases2 L3;
//    PSOM_phases2 LT;
//    EVSE2 evse;
//    float frequency;
//    float sensor_temperature;
//    float processor_temperature;
//    float circulationTime;
//    float circulationFrequency;
//    uint32_t uuid [4];
//};




enum PWM_STATE
{
    IDLE,
    STATE_A,                     //EV not connected
    STATE_B,                     //EV connected not ready
    STATE_C,                     //EV connected, charging, no ventilation
    STATE_D,                     //EV connected, charging, ventilation on (more power)
    STATE_E,                    //EVSE not available
    STATE_F                     //Grid not available
};

class Interface
{


private:
    PWM_STATE m_pwmstate;

    //    HarmonicType m_harmonictype;
    //    ReadingStates m_readingstates;
    //    PSOM_Voltage m_psom_voltage;
    //    PSOM_Current m_psom_current;
    //    PSOM_Energy m_psom_energy;
    //    PSOM_Harmonics m_psom_harmonics;
    //    PSOM_phases m_psom_phases;
    //    EVSE m_evse;


    PSOM_measurement_data m_psom_measurement_data;
    //    void* m_psom_measurement_data;
public:
    Interface();
    void setMeasurementData(PSOM_measurement_data data);
    //    void setMeasurementData(void* data);
    PSOM_measurement_data getMeasurementData();
    //    void* getMeasurementData();
    //    PWM_STATE getpwmstate();

//public slots:
//    void updatepsomdata(PSOM_measurement_data data);

};

#endif // INTERFACE_H
