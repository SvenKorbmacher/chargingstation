#ifndef SETTINGSPOPUP_H
#define SETTINGSPOPUP_H

#include <QWidget>

namespace Ui {
class SettingsPopup;
}

class SettingsPopup : public QWidget
{
    Q_OBJECT

public:
    explicit SettingsPopup(QWidget *parent = 0);
    ~SettingsPopup();

signals:
void ShowExpertMode();
void SafeToUsb();

private slots:

    void on_ExpertMode_released();



    void on_SafeToUsb_released();

private:
    Ui::SettingsPopup *ui;
};

#endif // SETTINGSPOPUP_H
