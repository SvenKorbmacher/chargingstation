#ifndef PWMFIG_H
#define PWMFIG_H

#include <QWidget>
#include <QWidget>
#include <QHBoxLayout>
#include <QTimer>
#include <QSize>
#include <QPushButton>
#include <QTableWidget>

#include "qOsci/qcustomplot.h"
#include "qOsci/qoscisignal.h"
#include "mData/mdatalogger.h"
#include "psomQt.h"


class pwmFig : public QWidget
{
    Q_OBJECT
public:
    explicit pwmFig(QWidget *parent = 0);
    QWidget      *getScreenWidget (void) { return screenWidget; }
    void            setScreenSize (QSize _screenSize);
//    void            setHarmonicsAxisStyle (OsciHarmAxisStyle style);
    void            setVerticalAxisStyle();

    void            osciStart (void);
    void            osciStop (void);
    void            osciReset (void);
    void            setInterface(PSOM* Interfaceobject);
    void            StopTimer();

private:
    QCustomPlot     *customPlot;
    QTimer          *osciTimer;
    QWidget         *screenWidget;
    QHBoxLayout     *screenLayout;
    int             SignalCount;

    PSOM* m_Interface;
    QTimer* Timer1;


     QCPBars *harmonicBars;
    qOsciSignal     * testSig1;
    qOsciSignal     * testSig2;
    void    initAsOsci      (void);

    void    initAsHarmonics (void);
//    OsciType                osciType;
//    OsciHarmAxisStyle       harmonicsAxisStyle;
//    OsciVerticalAxisStyle   verticalAxisStyle;
    QCPItemText *groupTracerText ;




signals:
    void osci_timeout (void);
public slots:
    void  setHarmonics (float *data, float freq, int count, int active);
    void updateOsci(mDataHandler *L1,mDataHandler *L2,mDataHandler *L3,mDataHandler *LT );
    void updateActualHarmonic (int actualHarm);
    void RebuildGraph();

private slots:
    void realtimeDataSlot (void);
};


#endif // PWMFIG_H
