#ifndef NUMPAD_H
#define NUMPAD_H

#include <QMainWindow>
#include <QApplication>
#include <QDialog>
#include <QString>
#include <QLineEdit>
#include <QPushButton>

class NumPad : public QDialog
{
    Q_OBJECT
public:
    explicit NumPad(QWidget *parent = 0);
    void retranslateUi(QDialog *n);
    ~NumPad() {}
    int getInt()           { return m_string.toUInt();   }
    double getDouble()     { return m_string.toDouble(); }
    QString getString()    { return m_string;}
private slots:
    void on_m_0_clicked()   { addChar('0');   }
    void on_m_1_clicked()   { addChar('1');   }
    void on_m_2_clicked()   { addChar('2');   }
    void on_m_3_clicked()   { addChar('3');   }
    void on_m_4_clicked()   { addChar('4');   }
    void on_m_5_clicked()   { addChar('5');   }
    void on_m_6_clicked()   { addChar('6');   }
    void on_m_7_clicked()   { addChar('7');   }
    void on_m_8_clicked()   { addChar('8');   }
    void on_m_9_clicked()   { addChar('9');   }
    void on_m_dot_clicked() { addChar('.');   }
    void on_m_ok_clicked()  { this->accept(); }
    void on_m_esc_clicked() { this->reject(); }
private:
    void addChar(char c)
    {
        m_string.append( c );
        m_display->setText(m_string);
    }
    QString m_string;
    QLineEdit   *m_display;
    QPushButton *m_0, *m_1, *m_2, *m_3, *m_4;
    QPushButton *m_5, *m_6, *m_7, *m_8, *m_9;
    QPushButton *m_dot, *m_ok, *m_esc;
};

#endif
