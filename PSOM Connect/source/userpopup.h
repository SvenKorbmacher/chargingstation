#ifndef USERPOPUP_H
#define USERPOPUP_H

#include <QWidget>


namespace Ui {
class UserPopup;
}

class UserPopup : public QWidget
{
    Q_OBJECT

public:
    explicit UserPopup(QWidget *parent = 0);
    void UpdateLabels(QString name, QString accessLevel, QString id);
//    void setLogin(LoginScreen* Login);
    ~UserPopup();

signals:
    void ShowLogin();

private slots:
   void on_UserLogut_released();

private:
    Ui::UserPopup *ui;
  //  LoginScreen* m_Login;



};

#endif // USERPOPUP_H
