# PowerLAB project file
# Nils Minor
#
# basic structure
# - PowerLAB
#   - PowerLAB
#       - gui
#       - source
#       - images
#   - qcustomplot
#

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport serialport

CONFIG  += no_batch
TARGET = PowerLab
TEMPLATE = app


target.path = /home/pi
INSTALLS += target

SOURCES += \
    source/console.cpp \
    source/main.cpp \
    source/mainwindow.cpp \
    source/settingsdialog.cpp \
    source/psomQt.cpp \
    source/psomQt_HAL.cpp \
    qOsci/qosci.cpp \
    qOsci/qoscisignal.cpp \
    qOsci/qcustomplot.cpp \
    mData/mdata.cpp \
    mData/mdatahandler.cpp \
    mData/mdatalogger.cpp \
    source/panel.cpp \
    source/oscilloscope.cpp \
    source/harmonics.cpp \
    source/logging.cpp \
    source/calibration.cpp \
    source/teamproject.cpp \
    source/serial.cpp \
    source/hmi.cpp \
    source/guihandler.cpp \
    source/interface.cpp \
    source/hmipopup.cpp \
    source/usagepopup.cpp \
    source/settingspopup.cpp \
    source/clockpopup.cpp \
    source/userpopup.cpp \
    source/numpad.cpp \
    source/mylineedit.cpp \
    source/loginscreen.cpp \
    source/pwmfig.cpp \
    source/database.cpp \
    source/user.cpp \
    source/datatransfer.cpp \
    source/loginmessage.cpp \
    source/gpioclass.cpp \
    source/mmapgpio.cpp


HEADERS += \
    source/console.h \
    source/mainwindow.h \
    source/settingsdialog.h \
    source/psomQt.h \
    source/PSOMRegsiter.h \
    source/psomQt_HAL.h \
    qOsci/qosci.h \
    qOsci/qoscisignal.h \
    qOsci/qcustomplot.h \
    mData/mdata.h \
    mData/mdatahandler.h \
    mData/mdatalogger.h \
    source/hmi.h \
    source/interface.h \
    waiting_for_psomdata.h \
    source/hmipopup.h \
    source/usagepopup.h \
    source/settingspopup.h \
    source/clockpopup.h \
    source/userpopup.h \
    source/numpad.h \
    source/mylineedit.h \
    source/loginscreen.h \
    source/pwmfig.h \
    source/database.h \
    source/user.h \
    source/datatransfer.h \
    source/loginmessage.h \
    source/gpioclass.h \
    source/mmapgpio.h


FORMS += \
    gui/mainwindow.ui \
    gui/settingsdialog.ui \
    source/hmi.ui \
    source/hmipopup.ui \
    source/usagepopup.ui \
    source/settingspopup.ui \
    source/clockpopup.ui \
    source/userpopup.ui \
    source/datatransfer.ui \
    source/loginscreen.ui \
    source/loginmessage.ui

RESOURCES += \
    PowerLAB.qrc
###############################################################################################

win32 {
    message(Build QWT500 Plugin only running on Windows)
    LIBS += -LC:/Users/Admin/ownCloud/Entwicklung/QWT500/QWT500/yokogawa/ -ltmctl64
    INCLUDEPATH += C:/Users/Admin/ownCloud/Entwicklung/QWT500/QWT500/
    SOURCES +=\
        C:/Users/Admin/ownCloud/Entwicklung/QWT500/QWT500/qwt500widget.cpp \
        C:/Users/Admin/ownCloud/Entwicklung/QWT500/QWT500/qwt500.cpp \
        C:/Users/Admin/ownCloud/Entwicklung/QWT500/QWT500/qwt500item.cpp

    HEADERS += \
           C:/Users/Admin/ownCloud/Entwicklung/QWT500/QWT500/qwt500widget.h \
           C:/Users/Admin/ownCloud/Entwicklung/QWT500/QWT500/qwt500.h \
           C:/Users/Admin/ownCloud/Entwicklung/QWT500/QWT500/qwt500item.h \
           C:/Users/Admin/ownCloud/Entwicklung/QWT500/QWT500/yokogawa/tmctl.h
    FORMS += C:/Users/Admin/ownCloud/Entwicklung/QWT500/QWT500/qwt500widget.ui
}

DISTFILES += \
    source/database.xml
